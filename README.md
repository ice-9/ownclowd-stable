# ownCloud install

## Описание
Репозиторий содержит ansible-playbook для установки ownCloud stable на удаленный хост Ubuntu14.04x64_server или Centos7x64.

В качестве веб-сервера используется nginx, база данных - postgresql.

После установки веб-интерфейс ownCloud доступен на http://host_ip/ с авторизацией admin:admin

## Требования

ansible v2

## Запуск

```
git clone git@gitlab.com:ice-9/ownclowd-stable.git
cd owncloud-stable
```

прописать в файл inventory ip-адрес целевого сервера и выполнить команду:

```
ansible-playbook playbook.yml -i inventory
```